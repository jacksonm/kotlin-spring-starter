CREATE TABLE "user"
(
    "user_pk"       UUID PRIMARY KEY,
    "first_name"    TEXT NOT NULL,
    "last_name"     TEXT NOT NULL,
    "email"         TEXT NOT NULL UNIQUE
);
