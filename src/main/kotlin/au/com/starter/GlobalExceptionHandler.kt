package au.com.starter

import au.com.starter.user.UserNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import javax.servlet.http.HttpServletResponse

@RestControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(value = [UserNotFoundException::class])
    fun entityNotFound(res: HttpServletResponse, e: Exception): ResponseEntity<ErrorDto> {
        logger.error(e.message)
        return ResponseEntity(ErrorDto(e.message), HttpStatus.NOT_FOUND)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(GlobalExceptionHandler::class.java)
    }

}
