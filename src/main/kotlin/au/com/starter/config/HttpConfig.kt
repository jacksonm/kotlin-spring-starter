package au.com.starter.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "http")
class HttpConfig(
        val maxNumberOfConnections: Int = 30,
        val maxConnectionsPerRoute: Int = 30,
        val socketTimeoutInMillisSeconds: Int = 60000,
        val connectionTimeoutInMillisSeconds: Int = 10000
)
