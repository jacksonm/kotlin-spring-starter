package au.com.starter.user

import java.util.*

data class UserDto(
    val userId: UUID,
    val firstName: String,
    val lastName: String,
    val email: String
)

fun UserEntity.toDto(): UserDto = UserDto(
    userId = userPk,
    firstName = firstName,
    lastName = lastName,
    email = email
)
