package au.com.starter.user

class UserNotFoundException(msg: String): RuntimeException("User not found: $msg")
