package au.com.starter.user

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class UserController(
    private val userService: UserService
) {

    @GetMapping("/v1/users")
    fun getUsers(): List<UserDto> = userService.getUsers()

    @GetMapping("/v1/users/{id}")
    fun getUser(@PathVariable("id") id: UUID): UserDto = userService.getUserById(id)

}
