package au.com.starter.user

import java.util.*
import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY

@Entity
@Table(name = "user")
data class UserEntity(
        @Id
        @Column(name = "user_pk")
        val userPk: UUID = UUID.randomUUID(),
        @Column(name = "first_name")
        val firstName: String,
        @Column(name = "last_name")
        val lastName: String,
        val email: String
)
