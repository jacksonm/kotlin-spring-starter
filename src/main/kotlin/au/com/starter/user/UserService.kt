package au.com.starter.user

import org.springframework.stereotype.Component
import java.util.*

@Component
class UserService(
    private val userRepository: UserRepository
) {

    fun getUsers(): List<UserDto> {
        val users = userRepository.findAll()
        return users.map { it.toDto() }
    }

    fun getUserById(id: UUID): UserDto {
        val user = userRepository.findByUserPk(id) ?: throw UserNotFoundException("No user with id $id")
        return user.toDto()
    }

}
