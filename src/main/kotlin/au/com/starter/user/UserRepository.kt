package au.com.starter.user

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : JpaRepository<UserEntity, UUID> {

    fun findByUserPk(id: UUID): UserEntity?

    fun findByEmail(email: String): UserEntity?

}
