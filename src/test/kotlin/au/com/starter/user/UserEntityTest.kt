package au.com.starter.user

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class UserEntityTest {

    private lateinit var user: UserEntity

    @Autowired
    private lateinit var userRepository: UserRepository

    @BeforeEach
    fun setUp() {
        userRepository.deleteAll()
        user = userRepository.save(UserEntity(firstName = "User", lastName = "Name", email = "user@example.com"))
    }

    @Test
    fun `should lookup user by email`() {
        assertThat(userRepository.findByEmail(user.email), `is`(notNullValue()))
    }

    @Test
    fun `should lookup user by id`() {
        assertThat(userRepository.findByUserPk(user.userPk), `is`(notNullValue()))
    }

}
