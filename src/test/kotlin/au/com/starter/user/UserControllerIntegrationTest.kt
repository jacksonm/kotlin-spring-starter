package au.com.starter.user

import com.nhaarman.mockitokotlin2.*
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

class UserControllerIntegrationTest : ControllerIntegrationTestBase() {

    @MockBean
    private lateinit var userRepository: UserRepository

    @Test
    fun `should retrieve all users`() {
        whenever(userRepository.findAll()).thenReturn(
            listOf(
                UserEntity(UUID.randomUUID(), firstName = "User", lastName = "Name", email = "user@example.com")
            )
        )

        doGet("/v1/users")
            .andExpect(status().isOk)
            .andExpect(jsonPath("$[0].email", `is`("user@example.com")))
    }

    @Test
    fun `should retrieve user by id`() {
        val userId = UUID.randomUUID()
        whenever(userRepository.findByUserPk(userId)).thenReturn(
            UserEntity(UUID.randomUUID(), firstName = "User", lastName = "Name", email = "user@example.com")
        )

        doGet("/v1/users/${userId}")
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.firstName", `is`("User")))
            .andExpect(jsonPath("$.lastName", `is`("Name")))
            .andExpect(jsonPath("$.email", `is`("user@example.com")))
    }

    @Test
    fun `should throw an exception when user cannot be found by id`() {
        whenever(userRepository.findByUserPk(any())).thenReturn(null)

        doGet("/v1/user/${UUID.randomUUID()}")
            .andExpect(status().isNotFound)
    }

}
