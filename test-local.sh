#!/bin/bash

set -e

trap_error() {
    docker-compose down
}

trap 'trap_error $LINENO' ERR

show_help() {
    cat << EOF
Runs the test suite locally.

-h, Display this help
-o, Starts the test db only (to run tests through IDE)
EOF
}

MODE="RUN-TESTS"

while getopts "ho" OPTION; do
    case ${OPTION} in
    o)
        MODE="NO-TESTS"
        ;;
    h)
        show_help
        exit 0
        ;;
    *)
        show_help
        exit 1
        ;;
    esac
done

echo "Mode is ${MODE}."

if [[ ${MODE} = "RUN-TESTS" ]]; then
    rm -rf /tmp/postgres-data
    docker-compose up -d test-db
    ./wait-for-log.sh ".*listening on IPv4 address \"0.0.0.0\".*" "test-db"
    mvn -Dflyway.url=jdbc:postgresql://localhost:${DB_PORT_TEST}/test \
      -Dflyway.locations=filesystem:src/main/resources/db/migration \
      flyway:migrate
    mvn clean install
    docker-compose down
else
    docker-compose up -d test-db
    ./wait-for-log.sh ".*listening on IPv4 address \"0.0.0.0\".*" "test-db"
    mvn -Dflyway.url=jdbc:postgresql://localhost:${DB_PORT_TEST}/test \
      -Dflyway.locations=filesystem:src/main/resources/db/migration \
      flyway:migrate
fi
