#!/bin/bash

while read LINE; do
    if [[ ${LINE} =~ ${1} ]]; then
        break
    fi
done < <(docker-compose logs -f ${2})