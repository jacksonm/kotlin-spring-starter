FROM adoptopenjdk/openjdk16-openj9:alpine-slim

MAINTAINER jacksonkmills@gmail.com

ADD ./target/starter-0.0.1-SNAPSHOT.jar /src/app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/src/app.jar"]
