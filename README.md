# Configuration for backend services

This starter to quickly spin up a new Kotlin + Spring boot server and supporting database. It contains:

* Spring Boot
* JPA + Hibernate
* PostgreSQL
* Flyway
* Junit
* Mockito

## Dependencies

* Maven
* direnv

## Configuration

`APP_NAME` configures the name of the service and dictates the name of the Docker containers and databases.
It is a good idea to have `APP_NAME`, `artifactId` and `name` matching.


Create a `.envrc` with the following variables:
```bash
export APP_NAME=kotlin-spring-starter
export DB_HOST=localhost
export DB_PORT=5432
export DB_PORT_TEST=5431
export DB_USERNAME=postgres
export DB_PASSWORD=password
export DB_SCHEMA=kotlin-spring-starter
export SERVER_PORT=8080

# required to target java SE 16 with kotlin 1.4.x 
export MAVEN_OPTS=--illegal-access=permit
```
Enabled with `direnv allow`

## Local environment
 - `./run-local.sh` starts the server
 - `./ci-local.sh` runs the test suite
 - `./ci-local.sh -o` starts the test database
