#!/bin/bash

while ! nc -z "$1" "$2";
do
  echo "Waiting for postgres ${1} service to start on port ${2}...";
  sleep 1;
done;
