#!/bin/bash

set -e
docker-compose up -d db
./wait-for-db.sh "${DB_HOST}" "${DB_PORT}"

mvn -Dflyway.url=jdbc:postgresql://"${DB_HOST}":"${DB_PORT}"/"${APP_NAME}" \
  -Dflyway.user=${DB_USERNAME} -Dflyway.password=${DB_PASSWORD} -Dflyway.schemas=${DB_SCHEMA} \
  flyway:migrate

mvn spring-boot:run
